import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/insurance",
    name: "Insurance",
    component: () => import("../views/incurance"),
    redirect: () => {
      return { name: 'InsuranceLanding' }
    },
    children: [
      {
        path: 'details/:id',
        name: 'Details',
        component: () => import("../components/insurance/InsuranceCard")
      },
      {
        path: 'home',
        name: 'InsuranceLanding',
        component: () => import("../components/insurance/Landing")
      },
      {
        path: 'search',
        name: 'InsuranceNumber',
        component: () => import("../components/insurance/InsuranceNumber")
      },
      {
        path: 'search-params',
        name: 'InsuranceParams',
        component: () => import("../components/insurance/InsuranceParams")
      }
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
