import Vue from 'vue'
import Vuex from 'vuex'
import insurance from './insurance'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    insurance
  }
})
