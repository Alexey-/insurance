import axios from 'axios'
const BASE_URL = 'https://test-ocean-credit-app.herokuapp.com/api/insurance'
const TOKEN = 'Token 38099b717b5722bcd13bafd9d96a0ecc8cf96b8a'
const FAKE_REQ = {
  auth_token: 'TK ToKeN',
  data: {
    'car 1': {
      car_number: 'AA1231OO',
      car_brand: 'Toyota',
      car_model: 'Land Cruiser',
      vin: 'WDD1693311J347156',
      insurance_policy: 'BB12343212',
      insurance_policy_status: 'Active',
      date_to: '2023-04-04T00:00:00Z',
      insurance_company: 'ООО Лучшая страховая',
      insurance_company_logo: 'http://strah.com/logo',
    },
    'car 2': {
      car_number: 'AA1233OO',
      car_brand: 'Audi',
      car_model: 'R8',
      vin: 'AAD1693311J227156',
      insurance_policy: 'BA12343232',
      insurance_policy_status: 'Finished',
      date_to: '2022-01-01T00:00:00Z',
      insurance_company: 'ООО Лучшая страховая',
      insurance_company_logo: 'http://strah.com/logo',
    }
  }
}

export default {
  state(){
    return {
      insurances: null
    }
  },
  mutations: {
    SET_INSURANCES(state, payload){
      state.insurances = payload
    }
  },
  getters: {
    getInsurances(state){
      return state.insurances
    }
  },
  actions: {
    async loadInsurances({ commit }){
      try{
        const req = await axios.post(BASE_URL,{
          auth_token: TOKEN
        },{
          headers: {
            'Authorization': TOKEN
          }
        })
        if(req.data){
          commit('SET_INSURANCES', req.data.data)
        }
      }
      catch(e){
        commit('SET_INSURANCES', FAKE_REQ.data)//only for test, api not ready
        console.error(e)
      }
    },
    async removeInsurence(_,number){
      try{
        await axios.post(BASE_URL+'/active/delete',{
          auth_token: TOKEN,
          car_number: number
        },{
          headers: {
            'Authorization': TOKEN
          }
        })
        return true
      }
      catch(e){
        console.error(e)
        return false
      }
    }
  }

}